const db = require('../database/database')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { variables } = require('../config/passport.js')
const { Op } = require('sequelize')
const crypto = require('crypto')

class UserService {
    static async get_self(id) {
        try {
            const user = await db.User.scope('noPassword').findOne({
                where: { idUser: id },
            })

            return user
        } catch (error) {
            throw error
        }
    }

    static async update_self(id, updateUser) {
        try {
            const userToUpdate = await db.User.findOne({
                where: { idUser: id },
            })

            if (userToUpdate) {
                const updatedUser = await db.User.update(updateUser, { returning: true, where: { idUser: id } })

                if (updatedUser[0] !== 0) return updatedUser[1][0].dataValues
            }
            return null
        } catch (error) {
            throw error
        }
    }

    static async delete_self(id) {
        try {
            const userToDelete = await db.User.findOne({
                where: { idUser: id },
            })

            if (userToDelete) {
                const deletedUser = await db.User.destroy({
                    where: { idUser: id },
                })
                await this.disconnect(id)
                return deletedUser
            }
            return null
        } catch (error) {
            throw error
        }
    }

    static async register(newUser) {
        try {
            const userToRegister = await db.User.findOne({
                where: {
                    [Op.or]: [{ username: { [Op.iLike]: newUser.username } }, { email: { [Op.iLike]: newUser.email } }],
                },
            })

            if (userToRegister) {
                if (newUser.username.toLowerCase() === userToRegister.username.toLowerCase()) return 'username'
                if (newUser.email.toLowerCase() === userToRegister.email.toLowerCase()) return 'email'
            } else {
                const createdUser = await db.User.create(newUser)
                const userIsMain = await db.UserIsMain.findOne({ where: { idHouse: newUser.idHouse } })
                let newUserIsMain = undefined
                if (!userIsMain)
                    newUserIsMain = await db.UserIsMain.create({
                        idUser: createdUser.idUser,
                        idHouse: newUser.idHouse,
                        isMain: true,
                    })
                else
                    newUserIsMain = await db.UserIsMain.create({
                        idUser: createdUser.idUser,
                        idHouse: newUser.idHouse,
                        isMain: false,
                    })
                return {
                    ...createdUser.dataValues,
                    isMain: newUserIsMain.dataValues.isMain,
                    isNewRecord: createdUser._options.isNewRecord,
                }
            }
        } catch (error) {
            console.log({ error })
            throw error
        }
    }

    static async login(userLogin, ipAdress) {
        try {
            const userToLogin = await db.User.findOne({
                where: {
                    [Op.or]: [
                        { username: { [Op.iLike]: userLogin.login } },
                        { email: { [Op.iLike]: userLogin.login } },
                    ],
                },
            })
            if (!userToLogin) return 'login'
            if (!bcrypt.compareSync(userLogin.password, userToLogin.dataValues.password)) return 'password'

            const userHouses = await db.UserIsMain.findAll({
                nest: true,
                raw: true,
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        attributes: ['nameHouse'],
                    },
                ],
                attributes: ['idHouse', 'isMain'],
                where: {
                    idUser: userToLogin.idUser,
                },
            })

            if (!userHouses) return 'house'

            let refreshToken = await create_or_update_refresh_token(userToLogin.dataValues, ipAdress)

            if (!refreshToken.token) refreshToken = refreshToken[1][0].dataValues

            const jwtToken = generate_jwt_token(userToLogin)
            delete userToLogin.dataValues.password

            return { user: userToLogin, jwtToken, houses: userHouses, refreshToken: refreshToken.token }
        } catch (error) {
            throw error
        }
    }

    static async refresh_token(token, ipAdress) {
        const refreshToken = await db.RefreshToken.findOne({
            where: { token },
        })
        if (!refreshToken || !refreshToken.isActive) return null
        return { jwtToken: generate_jwt_token(refreshToken) }
    }

    static async disconnect(idUser) {
        try {
            const disconnected = await db.RefreshToken.findOne({ where: { idUser: idUser } })
            if (disconnected) await db.RefreshToken.destroy({ where: { idUser: idUser } })

            return disconnected
        } catch (error) {
            throw error
        }
    }
}

function generate_jwt_token(user) {
    return jwt.sign({ sub: user.idUser, id: user.idUser }, variables.secretOrKey, {
        expiresIn: '1w',
    })
}

async function create_or_update_refresh_token(user, ipAddress) {
    const token = await db.RefreshToken.findOne({ where: { idUser: user.idUser } })
    if (token)
        return await db.RefreshToken.update(
            {
                token: crypto.randomBytes(40).toString('hex'),
                expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
                createdByIp: ipAddress,
            },
            { returning: true, where: { idUser: user.idUser } }
        )
    else
        return await db.RefreshToken.create({
            idUser: user.idUser,
            token: crypto.randomBytes(40).toString('hex'),
            expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000),
            createdByIp: ipAddress,
        })
}

module.exports = { UserService, generate_jwt_token }
