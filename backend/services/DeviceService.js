const db = require('../database/database')
const { Op } = require('sequelize')

class DeviceService {
    static async get_all_devices_of_user_from_house(idHouse, idUser) {
        try {
            const devices = await db.Device.findAll({
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        where: {
                            idHouse: idHouse,
                        },
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    [Op.and]: [{ idUser: idUser }, { idHouse: idHouse }],
                                },
                                nest: true,
                                raw: true,
                                attributes: [],
                            },
                        ],
                        nest: true,
                        raw: true,
                        attributes: [],
                    },
                    {
                        model: db.UserDevices,
                        as: 'device',
                        where: {
                            [Op.and]: [{ idUser: idUser }, { rightToWrite: true }],
                        },
                        nest: true,
                        raw: true,
                        attributes: [],
                    },
                ],
            })
            return devices
        } catch (error) {
            throw error
        }
    }

    static async get_device_from_house(idHouse, idUser, idDevice) {
        try {
            const device = await db.Device.findOne({
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        where: {
                            idHouse: idHouse,
                        },
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    [Op.and]: [{ idUser: idUser }, { idHouse: idHouse }],
                                },
                                nest: true,
                                raw: true,
                                attributes: [],
                            },
                        ],
                        nest: true,
                        raw: true,
                        attributes: [],
                    },
                    {
                        model: db.UserDevices,
                        as: 'device',
                        where: {
                            [Op.and]: [{ idUser: idUser }, { rightToWrite: true }],
                        },
                        nest: true,
                        raw: true,
                        attributes: [],
                    },
                ],
                where: { idDevice: idDevice },
            })
            return device
        } catch (error) {
            throw error
        }
    }

    static async add_device_to_house(idHouse, idDevice, idUser) {
        try {
            const house = await db.House.findOne({
                where: {
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.UserIsMain,
                        as: 'userIsMain',
                        where: {
                            [Op.and]: [{ idUser: idUser }, { idHouse: idHouse }],
                        },
                        nest: true,
                        raw: true,
                    },
                ],
                nest: true,
                raw: true,
                attributes: [],
            })
            if (!house) return 'house'

            const deviceExists = await db.Device.findOne({
                where: {
                    [Op.and]: [{ idDevice: idDevice }, { idHouse: idHouse }],
                },
            })
            if (deviceExists) {
                const userDeviceExists = await db.UserDevices.findOne({
                    where: {
                        [Op.and]: [{ idDevice: idDevice }, { idUser: idUser }],
                    },
                })
                if (!userDeviceExists) {
                    const newUserDevice = await db.UserDevices.create({
                        idDevice: idDevice,
                        idHouse: idHouse,
                        idUser: idUser,
                        rightToWrite: true,
                    })
                    return { idDevice, idHouse, idUser, rightToWrite: newUserDevice.rightToWrite }
                }
                return 'device'
            }
            const newDevice = await db.Device.create({
                idDevice: idDevice,
                idHouse: idHouse,
            })
            const newUserDevice = await db.UserDevices.create({
                idDevice: idDevice,
                idUser: idUser,
                rightToWrite: true,
            })
            return { idDevice, idHouse, idUser, rightToWrite: newUserDevice.rightToWrite }
        } catch (error) {
            throw error
        }
    }

    static async update_device_house(idHouse, idDevice, idUser) {
        try {
            const userRightsInDeviceHouse = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        where: { idHouse: idHouse },
                        nest: true,
                        raw: true,
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    idUser: idUser,
                                    idHouse: idHouse,
                                    isMain: true,
                                },
                            },
                        ],
                    },
                ],
            })

            if (!userRightsInDeviceHouse) 'oldHouse'

            const house = await db.House.findOne({
                where: {
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.UserIsMain,
                        as: 'userIsMain',
                        where: {
                            idUser: idUser,
                            idHouse: idHouse,
                            isMain: true,
                        },
                        nest: true,
                        raw: true,
                    },
                ],
                nest: true,
                raw: true,
                attributes: [],
            })
            if (!house) return 'newHouse'

            const userDevice = await db.UserDevices.findOne({
                where: {
                    idDevice: idDevice,
                    idUser: idUser,
                    rightToWrite: true,
                },
            })
            if (!userDevice) return 'device'

            const updated = await db.Device.update(
                {
                    idHouse: idHouse,
                },
                {
                    where: { idDevice: idDevice },
                    returning: true,
                }
            )
            if (updated[0] !== 0) return updated[1][0].dataValues
            return updated.dataValues
        } catch (error) {
            throw error
        }
    }

    static async update_device_right(idHouse, idDevice, idUser, idUserRight, rightToWrite) {
        try {
            const device = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                    idHouse: idHouse,
                },
            })
            if (!device) return 'device'

            const deviceRight = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.UserDevices,
                        as: 'device',
                        where: {
                            idUser: idUser,
                            idDevice: idDevice,
                            rightToWrite: true,
                        },
                    },
                ],
            })
            if (!deviceRight) return 'deviceRight'

            const userRightsInDeviceHouse = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        where: { idHouse: idHouse },
                        nest: true,
                        raw: true,
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    idUser: idUser,
                                    idHouse: idHouse,
                                    isMain: true,
                                },
                            },
                        ],
                    },
                ],
            })
            if (!userRightsInDeviceHouse) return 'house'

            const newUserRight = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                    idHouse: idHouse,
                },
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        where: { idHouse: idHouse },
                        nest: true,
                        raw: true,
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    idUser: idUserRight,
                                    idHouse: idHouse,
                                },
                            },
                        ],
                    },
                ],
            })
            if (!newUserRight) return 'houseUser'

            const userDevice = await db.UserDevices.findOne({
                where: {
                    [Op.and]: [{ idUser: idUserRight }, { idDevice: idDevice }],
                },
            })
            if (!userDevice) {
                const created = await db.UserDevices.create({
                    idUser: idUserRight,
                    idDevice: idDevice,
                    rightToWrite: rightToWrite,
                })
                return created
            }
            const updated = await db.UserDevices.update(
                {
                    rightToWrite: rightToWrite,
                },
                {
                    where: { [Op.and]: [{ idUser: idUserRight }, { idDevice: idDevice }] },
                    returning: true,
                }
            )
            if (updated[0] !== 0) return updated[1][0].dataValues
            return { ...updated.dataValues, idUserRight, idDevice, idHouse }
        } catch (error) {
            throw error
        }
    }

    static async delete_device(idDevice, idUser) {
        try {
            const findDevice = await db.Device.findOne({
                where: { idDevice: idDevice },
            })
            if (!findDevice) return 'noDevice'

            const userRightsInDeviceHouse = await db.Device.findOne({
                where: {
                    idDevice: idDevice,
                },
                include: [
                    {
                        model: db.House,
                        as: 'house',
                        nest: true,
                        raw: true,
                        include: [
                            {
                                model: db.UserIsMain,
                                as: 'userIsMain',
                                where: {
                                    [Op.and]: [
                                        {
                                            idUser: idUser,
                                            isMain: true,
                                        },
                                    ],
                                },
                            },
                        ],
                    },
                ],
            })
            if (!userRightsInDeviceHouse) 'house'

            const userDevice = await db.UserDevices.findOne({
                where: {
                    [Op.and]: [{ idUser: idUser }, { idDevice: idDevice }, { rightToWrite: true }],
                },
            })
            if (!userDevice) return 'device'

            const deletedDevice = await db.Device.destroy({
                where: { idDevice: idDevice },
            })
            return deletedDevice
        } catch (error) {
            throw error
        }
    }
}

module.exports = DeviceService
