'use strict'

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return await queryInterface.bulkInsert(
            'Houses',
            [
                {
                    nameHouse: 'House 1',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    nameHouse: 'House 2',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    nameHouse: 'House 3',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    nameHouse: 'House 4',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        )
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return await queryInterface.bulkDelete('Houses', null, {})
    },
}
