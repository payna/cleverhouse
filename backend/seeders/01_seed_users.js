'use strict'

module.exports = {
    up: async (queryInterface, Sequelize) => {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        return await queryInterface.bulkInsert(
            'Users',
            [
                {
                    username: 'Dylan',
                    email: 'dh@gmail.com',
                    password: '$2a$10$JPinbuYp/WS/OsaNRphO3OLB11BBc4G7X4eim1heuYO7Z62StItSW',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    username: 'Dylan2',
                    email: 'dh2@gmail.com',
                    password: '$2a$10$JPinbuYp/WS/OsaNRphO3OLB11BBc4G7X4eim1heuYO7Z62StItSW',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    username: 'Dylan3',
                    email: 'dh3@gmail.com',
                    password: '$2a$10$JPinbuYp/WS/OsaNRphO3OLB11BBc4G7X4eim1heuYO7Z62StItSW',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    username: 'Dylan4',
                    email: 'dh4@gmail.com',
                    password: '$2a$10$JPinbuYp/WS/OsaNRphO3OLB11BBc4G7X4eim1heuYO7Z62StItSW',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        )
    },

    down: async (queryInterface, Sequelize) => {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        return await queryInterface.bulkDelete('Users', null, {})
    },
}
