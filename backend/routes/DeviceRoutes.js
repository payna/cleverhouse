const Router = require('express')
const DeviceController = require('../controllers/DeviceController')
const passport = require('../config/passport')
const Util = require('../utils/Utils')
const util = new Util()

const router = Router()

router.get('/favicon.ico', (req, res) => res.status(204))

router.get('/house/', passport.authorized, (req, res) => {
    util.setError(400, `Please provide an idHouse`)
    return util.send(res)
})
router.get('/house/:idHouse', passport.authorized, DeviceController.get_all_devices_of_user_from_house)

router.get('/:idDevice/house/', passport.authorized, (req, res) => {
    util.setError(400, `Please provide an idHouse`)
    return util.send(res)
})
router.get('/:idDevice/house/:idHouse', passport.authorized, DeviceController.get_device_from_house)

router.post('/house/', passport.authorized, (req, res) => {
    util.setError(400, `Please provide an idHouse`)
    return util.send(res)
})
router.post('/house/:idHouse', passport.authorized, DeviceController.add_device_to_house)

router.put('/:idDevice/user/', passport.authorized, (req, res) => {
    util.setError(400, `Please provide an idUser`)
    return util.send(res)
})
router.put('/:idDevice/user/:idUser', passport.authorized, DeviceController.update_device_right)

router.put('/', passport.authorized, (req, res) => {
    util.setError(400, `Please provide an idDevice`)
    return util.send(res)
})
router.put('/:idDevice', passport.authorized, DeviceController.update_device_house)

router.delete('/', (req, res) => {
    util.setError(400, `Please provide an id`)
    return util.send(res)
})
router.delete('/:id', passport.authorized, DeviceController.delete_device)

module.exports = router
