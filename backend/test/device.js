//During the test the env variable is set to test
require('dotenv').config()
process.env.NODE_ENV = 'test'

let server = require('../app')

//Require the dev-dependencies
let chai = require('chai')
let chaiHttp = require('chai-http')
let should = chai.should()

chai.use(chaiHttp)

const db = require('../database/database')
db.sequelize.options.logging = false

const API_URL = '/devices'

const jwt = require('jsonwebtoken')
const { variables } = require('../config/passport.js')

let idDevice = 5
let idHouse = 0
let idUser = 0
let idUser_2 = 0
let jwtToken = ''
let jwtToken_2 = ''

const user = {
    username: 'testCreate',
    email: 'testCreate@clvrhouse.com',
    password: 'testPwd',
}

//Our parent block
describe('Devices', () => {
    before(async () => {
        await db.Device.destroy({ where: {}, truncate: { cascade: true } })
        await db.User.destroy({ where: {}, truncate: { cascade: true } })
        await db.House.create({ nameHouse: 'test_house' }).then(res => {
            idHouse = parseInt(res.dataValues.idHouse)
        })
        await db.User.create({ ...user, idHouse }).then(async res => {
            idUser = res.dataValues.idUser

            jwtToken = jwt.sign({ sub: idUser, id: idUser }, variables.secretOrKey, {
                expiresIn: '1w',
            })
            await db.UserIsMain.create({ idUser, idHouse, isMain: true }).then(async () => {
                await db.User.create({
                    username: 'testCreate2',
                    email: 'testCreate2@clvrhouse.com',
                    password: 'testPwd',
                    idHouse,
                }).then(async res => {
                    idUser_2 = res.dataValues.idUser

                    jwtToken_2 = jwt.sign({ sub: idUser_2, id: idUser_2 }, variables.secretOrKey, {
                        expiresIn: '1w',
                    })

                    await db.UserIsMain.create({ idUser: idUser_2, idHouse, isMain: false })
                })
            })
        })
    })
    beforeEach(async () => {
        await db.Device.destroy({ where: {}, truncate: { cascade: true } })
    })
    after(async () => {
        await db.Device.destroy({ where: {}, truncate: { cascade: true } })
        await db.User.destroy({ where: {}, truncate: { cascade: true } })
        await db.House.destroy({ where: {}, truncate: { cascade: true } })
    })

    describe('GET /house/:idHouse', () => {
        it('it should NOT GET device if token not provided', done => {
            chai.request(server)
                .get(`${API_URL}/house/${idHouse}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET device if token not valid', done => {
            chai.request(server)
                .get(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET device if idHouse not provided', done => {
            chai.request(server)
                .get(`${API_URL}/house/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    res.body.should.have.property('message').eql('Please provide an idHouse')
                    done()
                })
        })
        it('it should GET device', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.have.property('status').eql('success')

                    chai.request(server)
                        .get(`${API_URL}/house/${idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('array')
                            done()
                        })
                })
        })
    })

    describe('GET /:idDevice/house/:idHouse', () => {
        it('it should NOT GET device if token not provided', done => {
            chai.request(server)
                .get(`${API_URL}/${idDevice}/house/${idHouse}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET device if token not valid', done => {
            chai.request(server)
                .get(`${API_URL}/${idDevice}/house/${idHouse}`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT GET device if idHouse not provided', done => {
            chai.request(server)
                .get(`${API_URL}/${idDevice}/house/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should GET device', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .get(`${API_URL}/${idDevice}/house/${idHouse}`)
                        .send({ idDevice })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            res.body.data.should.have.property('idDevice').that.is.a('number')
                            res.body.data.should.have.property('idHouse').that.is.a('number')
                            done()
                        })
                })
        })
    })

    describe('POST /house/:idHouse', () => {
        it('it should NOT POST device if token not provided', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST device if token not valid', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT POST device if idHouse not provided', done => {
            chai.request(server)
                .post(`${API_URL}/house/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it("it should NOT POST device if user doesn't exist in provided house", done => {
            chai.request(server)
                .post(`${API_URL}/house/10`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(409)
                    res.body.should.have.property('status').eql('error')
                    res.body.should.have.property('message').eql("User doesn't exist in house")
                    done()
                })
        })
        it('it should NOT POST device if device already exists', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.have.property('status').eql('success')

                    chai.request(server)
                        .post(`${API_URL}/house/${idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idDevice })
                        .end((err, res) => {
                            res.should.have.status(403)
                            res.body.should.have.property('status').eql('error')
                            res.body.should.have.property('message').eql(`${idDevice} already exists`)
                            done()
                        })
                })
        })
        it('it should POST device', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(201)
                    res.body.should.have.property('status').eql('success')
                    res.body.should.have.property('data').that.is.an('object')
                    res.body.data.should.have.property('idDevice').that.is.a('number').eql(idDevice)
                    res.body.data.should.have.property('idHouse').that.is.a('number').eql(idHouse)
                    res.body.data.should.have.property('idUser').that.is.a('number').eql(idUser)
                    res.body.data.should.have.property('rightToWrite').that.is.a('boolean').eql(true)

                    done()
                })
        })
    })

    describe('PUT /:idDevice/user/:idUser', () => {
        it('it should NOT PUT device if token not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if token not valid', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if idDevice not provided', done => {
            chai.request(server)
                .put(`${API_URL}//user/${idUser}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    done()
                })
        })
        it('it should NOT PUT device if idUser not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}/user/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if idHouse not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    res.body.should.have.property('message').eql('Please provide an idHouse')
                    done()
                })
        })
        it('it should NOT PUT device if rightToWrite not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idHouse })
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    res.body.should.have.property('message').eql('Please provide a rightToWrite')
                    done()
                })
        })
        it("it should NOT PUT device if user doesn't have right in oldHouse", done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .post(`${API_URL}/house/${idHouse}`)
                        .set('Authorization', 'Bearer ' + jwtToken_2)
                        .send({ idDevice })
                        .end((err, res) => {
                            chai.request(server)
                                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                                .set('Authorization', 'Bearer ' + jwtToken_2)
                                .send({ idHouse, rightToWrite: true })
                                .end((err, res) => {
                                    res.should.have.status(409)
                                    res.body.should.have.property('status').eql('error')
                                    res.body.should.have
                                        .property('message')
                                        .eql("User doesn't have rights in the house")

                                    done()
                                })
                        })
                })
        })
        it("it should NOT PUT device if user doesn't exist in newHouse", done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .put(`${API_URL}/${idDevice}/user/${idUser_2 + 1}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idHouse, rightToWrite: true })
                        .end((err, res) => {
                            res.should.have.status(409)
                            res.body.should.have.property('status').eql('error')
                            res.body.should.have.property('message').eql("User selected doesn't exist in the house")

                            done()
                        })
                })
        })
        it("it should NOT PUT device if device doesn't exist", done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice - 1}/user/${idUser}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idHouse, rightToWrite: true })
                .end((err, res) => {
                    res.should.have.status(403)
                    res.body.should.have.property('status').eql('error')
                    res.body.should.have.property('message').eql("Device doesn't exist")
                    done()
                })
        })
        it("it should NOT PUT device if user doesn't have rights on device", done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .put(`${API_URL}/${idDevice}/user/${idUser_2}`)
                        .send({ idHouse, rightToWrite: false })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            chai.request(server)
                                .put(`${API_URL}/${idDevice}/user/${idUser}`)
                                .send({ idHouse, rightToWrite: false })
                                .set('Authorization', 'Bearer ' + jwtToken_2)
                                .end((err, res) => {
                                    res.should.have.status(409)
                                    res.body.should.have.property('status').eql('error')
                                    res.body.should.have
                                        .property('message')
                                        .eql(`User doesn\'t have rights to modify device`)

                                    done()
                                })
                        })
                })
        })
        it('it should PUT device', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .put(`${API_URL}/${idDevice}/user/${idUser_2}`)
                        .send({ idHouse, rightToWrite: false })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            res.body.should.have.property('data').that.is.an('object')
                            res.body.should.have
                                .property('message')
                                .eql(`Device ${idDevice} changed right to ${false} for user ${idUser_2}!`)
                            res.body.data.should.have.property('rightToWrite').eql(false)

                            done()
                        })
                })
        })
    })

    describe('PUT /:idDevice', () => {
        it('it should NOT PUT device if token not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if token not valid', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}`)
                .send({ idDevice })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if idDevice not provided', done => {
            chai.request(server)
                .put(`${API_URL}/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT PUT device if idHouse not provided', done => {
            chai.request(server)
                .put(`${API_URL}/${idDevice}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should PUT device', done => {
            let newHouse_id = 0
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .post(`/houses`)
                        .send({ nameHouse: 'newHouseToPut' })
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .end((err, res) => {
                            newHouse_id = res.body.data.house.idHouse
                            chai.request(server)
                                .put(`${API_URL}/${idDevice}`)
                                .send({ idHouse: newHouse_id })
                                .set('Authorization', 'Bearer ' + jwtToken)
                                .end((err, res) => {
                                    res.should.have.status(200)
                                    res.body.should.have.property('status').eql('success')
                                    res.body.should.have
                                        .property('message')
                                        .eql(`Device ${idDevice} changed to house ${newHouse_id}!`)
                                    done()
                                })
                        })
                })
        })
    })

    describe('DELETE /:id', () => {
        it('it should NOT DELETE device if token not provided', done => {
            chai.request(server)
                .delete(`${API_URL}/${idDevice}`)
                .send({ idDevice })
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE device if token not valid', done => {
            chai.request(server)
                .delete(`${API_URL}/${idDevice}`)
                .send({ idDevice })
                .set('Authorization', 'Bearer a' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(401)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE device if device id not provided', done => {
            chai.request(server)
                .delete(`${API_URL}/`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should NOT DELETE device if device not found', done => {
            chai.request(server)
                .delete(`${API_URL}/${idDevice + 1}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .end((err, res) => {
                    res.should.have.status(404)
                    res.body.should.have.property('status').eql('error')
                    done()
                })
        })
        it('it should DELETE device', done => {
            chai.request(server)
                .post(`${API_URL}/house/${idHouse}`)
                .set('Authorization', 'Bearer ' + jwtToken)
                .send({ idDevice })
                .end((err, res) => {
                    chai.request(server)
                        .delete(`${API_URL}/${idDevice}`)
                        .set('Authorization', 'Bearer ' + jwtToken)
                        .send({ idDevice })
                        .end((err, res) => {
                            res.should.have.status(200)
                            res.body.should.have.property('status').eql('success')
                            done()
                        })
                })
        })
    })
})
