'use strict'
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('RefreshTokens', {
            idRefreshToken: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            token: {
                type: Sequelize.STRING(100),
                allowNull: false,
            },
            expires: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            createdByIp: {
                allowNull: true,
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        })
        await queryInterface.addColumn('RefreshTokens', 'idUser', {
            type: Sequelize.INTEGER,
            references: { model: 'Users', key: 'idUser' },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        })
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('RefreshTokens')
    },
}
