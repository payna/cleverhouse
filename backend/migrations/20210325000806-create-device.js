'use strict'
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable(
            'Devices',
            {
                idDevice: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER,
                },
                idHouse: {
                    allowNull: false,
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Houses',
                        key: 'idHouse',
                    },
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
            },
            {
                uniqueKeys: {
                    unique_tag: {
                        customIndex: true,
                        fields: ['idDevice', 'idHouse'],
                    },
                },
            }
        )
        await queryInterface.createTable(
            'UserDevices',
            {
                idUserDevices: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: Sequelize.INTEGER,
                },
                idDevice: {
                    allowNull: false,
                    type: Sequelize.INTEGER,
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                    references: {
                        model: 'Devices',
                        key: 'idDevice',
                    },
                },
                idUser: {
                    allowNull: false,
                    type: Sequelize.INTEGER,
                    references: {
                        model: 'Users',
                        key: 'idUser',
                    },
                    onDelete: 'CASCADE',
                    onUpdate: 'CASCADE',
                },
                rightToWrite: {
                    allowNull: false,
                    type: Sequelize.BOOLEAN,
                    defaultValue: false,
                },
                createdAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
                updatedAt: {
                    allowNull: false,
                    type: Sequelize.DATE,
                },
            },
            {
                uniqueKeys: {
                    unique_tag: {
                        customIndex: true,
                        fields: ['idUser', 'idDevice'],
                    },
                },
            }
        )
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('Devices')
        await queryInterface.dropTable('UserDevices')
    },
}
