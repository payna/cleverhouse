import os
import json
import pprint
import random 
with open("./log_eg.json") as f :
    d = json.loads(f.read())

logid = [random.randint(0,100) for i in range(0,25)]

dates = ["2021-22-11 14:00:00", "2021-22-11 15:00:00", "2021-22-11 16:00:00", "2021-22-11 17:00:00", "2021-22-11 18:00:00", "2021-22-11 19:00:00"]

houseId = [str(random.randint(0,100)) for i in range(0,25)]

userId = [random.randint(0,100) for i in range(0,25)]

colors = ["#677633", "#4587653","#087676", "#098753", "#986456"]

randomint = [random.randint(0,100) for i in range(0,25)]

unite = ["%","C", "F","lumen"]

randomintstr = [str(random.randint(0,100)) for i in range(0,25)]

devicesnames = ["light1","light2","light3","light4","heat1","heat2","heat3","heat4"]


print("logid", logid)

print("dates", dates)

print("houseId",houseId)

print("userId", userId)

print("colors",colors)

print("randomint",randomint)

print("unite",unite)

print("randomintstr",randomintstr)

print("devicesnames",devicesnames)


full_dic = {}
for i in range(0,len(logid)):
    states = random.choice([True,False])
    system = {}

    for j in range(0,len(devicesnames)):
        a = {
            "state" : random.choice([True,False]),
            "intensity" : random.choice(randomint),
            "color" : random.choice(colors)
        }
        system[j] = a

    dic = {
        str(logid[i]) : {
            "meta" : {
                "houseId" : random.choice(houseId),
                "userChange" : random.choice(userId),
                "date" : random.choice(dates)
            },

            "deviceChanged" : {
                "id" : random.choice(devicesnames),
                "precState" : states,
                "newState" : not states
            },

            "system": system


        }
    }

    full_dic[list(dic.keys())[0]] = dic[list(dic.keys())[0]]


pprint.pprint(full_dic)
    


f = open("./log_full.json",'w')
json.dump(full_dic,f,separators=(",",": "),indent=4, sort_keys=True )
f.close()

