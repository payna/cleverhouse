import json
import pprint

def test():
    with open('log_full.json') as a:
        dict1 = json.load(a)

    features=[]
    label=[]


    for i in dict1.keys():
        tablo=[]
        newState=dict1[i]["deviceChanged"]["newState"]
        precState=dict1[i]["deviceChanged"]["precState"]
        date=dict1[i]["meta"]["date"]
        userChange=dict1[i]["meta"]["userChange"]
        label.append(newState)
        tablo.append(precState)
        tablo.append(date)
        tablo.append(userChange)
        for j in dict1[i]["system"].keys():
            color=dict1[i]["system"][j]["color"]
            intensity=dict1[i]["system"][j]["intensity"]
            state=dict1[i]["system"][j]["state"]
            tablo.append(color)
            tablo.append(intensity)
            tablo.append(state)

        features.append(tablo)

    print(len(features))
    print(len(label))
    return features,label