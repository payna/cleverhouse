from typing import Any, Optional, List

from fastapi import FastAPI

from models.device import Device
from models.mesure import Mesure
from models.room import Room

app = FastAPI()

devices = [
    {"idDevice": 1, "deviceName": "Lampe"},
    {"idDevice": 2, "deviceName": "Volet"},
]

rooms = [
    {"idRoom": 1, "roomName": "Chambre"},
    {"idRoom": 2, "roomName": "Cuisine"},
]

mesures = [
    {"unite": "°C", "idDevice": 1, "typeMesure": "", "valueMesure": "10"},
    {"unite": "°C", "idDevice": 2, "typeMesure": "", "valueMesure": "12"},
]

## ROUTE TEST

@app.get("/api/")
def read_root():
    return {"Code": "200"}

## GET COLLECTION ROUTES 

@app.get("/api/devices/", response_model=List[Device])
async def read_devices():
    ##devices = await db["devices"].find().to_list(1000)
    return devices

@app.get("/api/rooms/", response_model=List[Room])
async def read_rooms():
    return rooms

@app.get("/api/mesures/", response_model=List[Mesure])
async def read_mesures():
    return mesures


## GET ITEM ROUTES 

@app.get("/api/device/{device_id}")
def read_device_item(device_id: int, q: Optional[str] = None):
    return {"device_id": device_id, "q": q}

@app.get("/api/room/{room_id}")
def read_room_item(room_id: int, q: Optional[str] = None):
    return {"room_id": room_id, "q": q}

@app.get("/api/mesure/{mesure_id}")
def read_mesure_item(mesure_id: int, q: Optional[str] = None):
    return {"mesure_id": mesure_id, "q": q}

@app.get("/api/mesure/{mesure_id}/device/{device_id}")
def read_mesure_of_device(mesure_id: int, q: Optional[str] = None):
    return {"mesure_id": mesure_id, "q": q}

@app.get("/api/device/{device_id}/room/{room_id}")
def read_device_of_room(room_id: int, q: Optional[str] = None):
    return {"room_id": room_id, "q": q}

## POST ROUTES 

@app.post("/device")
async def create_device(device: Device):
    return device

@app.post("/room")
async def create_room(room: Room):
    return room

@app.post("/mesure")
async def create_mesure(mesure: Mesure):
    return mesure

## PUT ITEM ROUTES 

@app.put("/device/{idDevice}")
def update_device(idDevice: int, device: Device):
    return {"deviceName": device.deviceName, "idDevice": idDevice}

@app.put("/room/{idRoom}")
def update_room(idRoom: int, room: Room):
    return {"roomName": room.roomName, "idRoom": idRoom}

@app.put("/mesure/{idMesure}")
def update_mesure(idMesure: int, mesure: Mesure):
    return {"idMesure": idMesure, "typeMesure": mesure.typeMesure, "unite": mesure.unite, "valueMesure": mesure.valueMesure}
