from typing import List


from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Mesure(BaseModel):
    unite: str
    idDevice: int
    typeMesure: str
    valueMesure: str
